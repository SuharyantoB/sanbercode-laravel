<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function index()
    {
        $casts= Cast::all();
        return view('cast.tampil', ['casts' => $casts]);
    }

    public function create()
    {
        return view('cast.tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required', 
        ]);

        Cast::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $Cast = Cast::find($id);
        return view('cast.detail', ['Cast' => $Cast]);
    }

    public function edit($id)
    {
        $Cast = Cast::find($id);
        return view('cast.update', ['Cast' => $Cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        Cast::where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);
            return redirect('/cast');
    }

    public function destroy($id)
    {
        $Cast = Cast::find($id);
 
        $Cast->delete();

        return redirect('/cast');
    }

}
