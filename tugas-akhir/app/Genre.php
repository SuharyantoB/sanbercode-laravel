<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genres';
    protected $fillable = ['nama'];

    public function film()
    {
        return $this->hasMany(Film::class, 'genre_id');
    }
}
