<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    public function index()
    {
        $genres = Genre::get();
        return view('genre.index', ['genres' => $genres]);
    }

    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Genre::create([
            'nama' => $request['nama'], 
        ]);

        return redirect('/genre');
    }

    public function show($id)
    {
        $Genre = Genre::find($id);
        return view('genre.show', ['Genre' => $Genre]);
    }

    public function edit($id)
    {
        $Genre = Genre::find($id);
        return view('genre.edit', ['Genre' => $Genre]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        Genre::where('id', $id)
            ->update([
                'nama' => $request['nama'],
                
            ]);
            return redirect('/genre');
    }

    public function destroy($id)
    {
        $Genre = Genre::find($id);
 
        $Genre->delete();

        return redirect('/genre');
    }

}


